# Overview

This is simple app to test deployments using Gitlab CI and Kubernetes.

# Requirements

Go to your Gitlab project Settings -> CI/CD -> Variables.

Setup the following variables:

* DOCKER_REGISTRY: the docker registry you want to send the artifact (docker image) to.

* DOCKER_REPO: the docker repo where the image will be pushed to.

* DOCKER_USER: your docker registry user name.

* DOCKER_PASSWORD: your docker registry password.

* kube_config: your K8s cluster kubeconfig base64 encoded. Use the follwing comand and get the output: `cat kubeconfig | base64`.

* AWS_ACCESS_KEY_ID: AWS credentials to access EKS.

* AWS_SECRET_ACCESS_KEY: AWS credentials to access EKS.

* AWS_DEFAULT_REGION: AWS credentials to access EKS.

